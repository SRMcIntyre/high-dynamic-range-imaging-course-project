/* 
HDR2.cc
For Digital Image Processing Independent Study
Getting Points where point is obtained when (i*j)%nPts==0
By Stephanie McIntyre
*/

// INCLUDES
#include <netpbm/pam.h>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <utility>
#include <algorithm>
#include <vector>
#include <fstream>
#include "HDR.h"

using namespace std;
using namespace Eigen;

// MAIN PROGRAM
int main(int argc, char *argv[])
{ 
  /* Structures for Input/Output Image */
  pam inpam; tuple **array;
  
  /* Initialize Library */
  pm_init(argv[0], 0);
 
  /* Read Image */
  array = read_image(argv[1], inpam);

  //Variables
  int nPics=atoi(argv[2]), nPts=30, n=256;
  double lambda=atoi(argv[4]);
  int row=nPts * nPics + n + 1, col= n + nPics;
  double *** all_values = new double ** [inpam.height];
  int k=nPics*nPts;
  
  for (int i=0; i<inpam.height; i++)
    {
      all_values[i] = new double * [inpam.width];
      for (int j=0; j<inpam.width; j++)
	all_values[i][j] = new double [nPics];
    }

  //Matrix & Vectors for SVD
  MatrixXf A = MatrixXf::Zero(row, col);
  VectorXf b = VectorXf::Zero(row);
  
  /* Find pixel points */
  pair<int, int> * pts = new pair<int,int> [nPts];
  get_points2(array, inpam, nPts, pts);
  
  double ** values = fill_Ms(array, A, b, nPics, nPts, pts, all_values, inpam.height, inpam.width);

  A(k, 129) = 1;
  k++;
  
  //Smoothness
  smoothness(A, lambda, k);

  //SVD
  JacobiSVD <MatrixXf> svd(A, ComputeThinU | ComputeThinV);
  VectorXf x = svd.solve(b);
  double *g = new double[n];
  
  for (int i=0; i<n; i++)
    g[i] = x[i];

  double minIrr=1000, maxIrr=-1;
  lnE(g, values,  nPics, nPts, all_values, inpam.height, inpam.width, minIrr, maxIrr);
  scale(all_values, array, inpam.height, inpam.width, minIrr, maxIrr);

  /* Write Image Information */
  write_image(argv[3], inpam, array);
  writeArray(g);

  /* Clean Up */
  for (int i=0; i<inpam.height; i++)
    {
      for (int j=0; j<inpam.width; j++)
	delete [] all_values[i][j];
      delete [] all_values[i];
    }
  delete [] all_values;
  delete [] g;
  delete [] pts;
  pnm_freepamarray(array, &inpam);
  
  return 0;
}
