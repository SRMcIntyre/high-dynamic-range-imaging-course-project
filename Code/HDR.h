#ifndef _HDR_H_
#define _HDR_H_

#include <netpbm/pam.h>
#include <Eigen/Dense>
#include <Eigen/SVD>

using namespace std;
using namespace Eigen;

tuple **read_image(char *filename, pam &inpam);
void write_image(char *filename, const pam &inpam, tuple **array);
void get_points(tuple ** data, const pam &inpam, int &nPts,
		pair<int, int> * pts);
void get_points2(tuple **data, const pam &inpam, int &nPts, pair<int, int>* points);
double** fill_Ms(tuple **array, MatrixXf &A, VectorXf &b, int nPics, int nPts,
		 pair <int, int>* pts, double *** values, int h, int w);
void fill_matrix(double ** values, int nPics, int nPts, MatrixXf&A, VectorXf&b);
void smoothness(MatrixXf& A, double lambda, int k);
double w(double a);
void lnE(double* g, double ** values, int nPics, int nPts,
	 double *** all_values,int m, int n, double & minIrr, double & maxIrr);
void scale(double *** values, tuple ** array, int h, int w,
	   double minIrr, double maxIrr);
void writeArray(double* ret);
void writeArrayLNE(double* ret);
  
#endif
