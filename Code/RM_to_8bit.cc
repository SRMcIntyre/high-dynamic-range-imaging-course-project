/* 
RM_to_8bit.cc
For Digital Image Processing Independent Study
Converts the Radiance Map into an 8 Bit Image
By Stephanie McIntyre
*/

// INCLUDES
#include <netpbm/pam.h>
#include <iostream>
#include <cmath>
#include "HDR.h"

using namespace std;

// MAIN PROGRAM
int main(int argc, char *argv[])
{ 
  /* Structures for Input/Output Image */
  pam inpam; tuple **array;
  
  /* Initialize Library */
  pm_init(argv[0], 0);

  /* Read Image */
  array = read_image(argv[1], inpam);

  /* Variables */
  double time = atoi(argv[2]);
  time = log(1/time);
  
  /* Read in g(Z) */
  double g[256]={};
  double gMax=-1, gMin=1000;
  for (int i=0; i<256; i++)
    {
      cin >> g[i];
      if (g[i]>gMax)
	gMax = g[i];
      else if (g[i]<gMin)
	gMin = g[i];
      g[i] = g[i] - time;
    }

  double fac = exp(gMax-gMin-time)/255.0;
  /* Assign Values based off Equation 5 */
  for (int i=0; i<inpam.height; i++)
    for (int j=0; j<inpam.width; j++)
      {
	int a = (array[i][j][0]+array[i][j][1]+array[i][j][2])/3.0;
	double b = g[a];
	int c = exp(b)/fac;
	if(c<0)
	  c=0;
	else if(c>255)
	  c=255;
	array[i][j][0]=c; array[i][j][1]=c; array[i][j][2]=c;
      }

  writeArrayLNE(g);
  
  /* Write Image */
  write_image(argv[3], inpam, array);

  /* Clean Up */
  pnm_freepamarray(array, &inpam);
}
