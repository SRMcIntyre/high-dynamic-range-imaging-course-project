#include "HDR.h"

#include <netpbm/pam.h>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <utility>
#include <algorithm>
#include <vector>
#include <fstream>

using namespace std;
using namespace Eigen;

// FUNCTION scale
/* outputs an 8 bit image */
void scale(double *** values, tuple ** array, int h, int w, double minIrr, double maxIrr)
{
  double fac = (maxIrr-minIrr)/255.0;

  for (int i=0;i<h;i++)
    for (int j=0;j<w;j++)
      {
	int v = values[i][j][0]/fac;
	if (v>255)
	  v=255;
	array[i][j][0]=v; array[i][j][1]=v; array[i][j][2]=v;
      }
}

// FUNCTION lnE
/* calculates based on Equation 6 */
void lnE(double* g, double ** values, int nPics, int nPts, double *** all_values,int m, int n, double & minIrr, double & maxIrr)
{
  for (int i=0; i<m; i++)
      for (int k=0; k<n; k++)
	{
	  double wij=0;
	  double top=0;

	  for (int j=0; j<nPics; j++)
	    {
	      double temp = all_values[i][k][j];
	      temp = w(temp);
	      top+=(g[static_cast<int>(temp)]-values[j][nPts])*temp;
	      wij+=temp;
	    }
	  
	  all_values[i][k][0]=exp(top/wij);
	  if (all_values[i][k][0]>maxIrr)
	    maxIrr = all_values[i][k][0];
	  else if (all_values[i][k][0]<minIrr)
	    minIrr = all_values[i][k][0];
	}
}

// FUNCTION writeArray
/* writes g function values */
void writeArray(double* ret)
{
  ofstream file;
  file.open("g.txt");
  for (int i=0;i<256;i++)
    file << ret[i] << endl;
  file.close();
}

// FUNCTION writeArray
/* writes g function values */
void writeArrayLNE(double* ret)
{
  ofstream file;
  file.open("lnE.txt");
  for (int i=0;i<256;i++)
    file << ret[i] << endl;
  file.close();
}

// FUNCTION read_image
/* reads the image */
tuple **read_image(char *filename, pam &inpam)
{
  FILE *f;
  tuple **A;

  if ((f = pm_openr(filename)) == NULL)
    {
      cerr << "Cannot open file \"" << filename << "\" for reading." << endl;
      exit(1);
    }

  if ((A = pnm_readpam(f, &inpam, PAM_STRUCT_SIZE(tuple_type))) == NULL)
    {
      cerr << "Cannot read image \"" << filename << "\"." << endl;
      exit(1);
    }
  
  pm_close(f);
  return A;
}


// FUNCTION write_image
/* writes the image to the given file */
void write_image(char *filename, const pam &inpam, tuple **array)
{
  FILE *f;
  pam outpam = inpam;
  
  if ((f = pm_openw(filename)) == NULL)
    {
      cerr << "Cannot open file \"" << filename << "\" for writing." << endl;
      exit(1);
    }
  
  outpam.file = f;
  pnm_writepam(&outpam, array);
  
  pm_close(f);
}

// FUNCTION get_points
/* get variety of points based on Method 1 */
void get_points(tuple **array, const pam &inpam, int &nPts, pair<int, int>* points)
{
  const int NZs = 255/(nPts*1.0) + 0.5;
  int *Zs = new int [nPts];
 
  for (int i=0;i<nPts;i++)
    Zs[i]=0;
 
  int prevI=0, prevJ=0, p=0;
  bool done = false;

  for (int i=0; i<inpam.height; i++)
    {
      for (int j=0; j<inpam.width; j++)
	{
	  int value = (array[i][j][0]+array[i][j][1]+array[i][j][2])/3.0+0.5;
	  int Zs_t = value/(NZs*1.0);
	  int iDiff = i-prevI, jDiff = j-prevJ;
	  iDiff*=iDiff; jDiff*=jDiff;
	  
	  if (Zs[Zs_t]==0
	      && (sqrt(iDiff+jDiff)>20))
	    {
	      points[p]=make_pair(i, j);
	      Zs[Zs_t]=1;
	      prevI = i; prevJ = j;
	      p++;
	    }
	  
	  if (p==nPts)
	    {
	      done = true;
	      break;
	    }
	}
      if (done)
	break;
      }
  nPts=p;
  delete [] Zs;
}

//Function get_points2
/* get variety of points */
void get_points2(tuple **data, const pam &inpam, int &nPts, pair<int, int>* points)
{
  int bleh = inpam.height * inpam.width / nPts;
  int c = 0;
  int p = 0;
  for (int i=0; i<inpam.height; i++)
    {
      for (int j=0; j<inpam.width; j++)
	{
	  if (c%bleh==0)
	    {
	      points[p]=make_pair(i, j);
	      p++;
	    }
	  c++;
	}
      }
  nPts = p;
}

// FUNCTION fill_Ms
/* Fills all the data points */
double ** fill_Ms(tuple **array, MatrixXf &A, VectorXf &b, int nPics, int nPts, pair <int, int>* pts, double *** all_values, int h, int w)
{
  pam inpam;
  double ** values= new double*[nPics];
  for (int j=0;j<nPics;j++)
    {
      values [j]=new double[nPts+1];
      string f; double time;
      cin >> f >> time;
      char *y = new char[f.length()+1];
      strcpy(y, f.c_str());
      
      array = read_image(y, inpam);

      for (int i=0;i<h;i++)
	  for (int k=0;k<w;k++)
	      all_values[i][k][j] =
		(array[i][k][0]+array[i][k][1]+array[i][k][2])/3.0+0.5;

      for (int i=0;i<nPts;i++)
	{
	  int f = pts[i].first, s = pts[i].second;
	  values[j][i]=(array[f][s][0]+array[f][s][1]+array[f][s][2])/3.0+0.5;
	}
      
      time = 1/time;
      values[j][nPts]=log(time);
      delete [] y;
    }
  
  fill_matrix(values, nPics, nPts, A, b);

  return values;
}

// FUNCTION w
/* returns value for Equation 4 */
double w(double a)
{
  if (a<128)
    return a;
  return 256-a;
}

// FUNCTION fill_matrix
/* fills matrix A and vector B */
void fill_matrix(double ** values, int nPics, int nPts, MatrixXf&A, VectorXf&b)
{
  int n=257;
  int k=0;
 
  for (int i=0; i<nPts; i++)
      for (int j=0; j<nPics; j++)
	{
	  int Zij=static_cast<int>(values[j][i])+1;
	  double wij = w(Zij);
	  A(k, Zij+1) = wij;
	  A(k, n+1) = -1.0*wij;
	  double time=values[j][nPts];
	  b(k) = wij * time;
	  k++;
	}
}

// FUNCTION smoothness
/* Takes care of smoothness function */
void smoothness(MatrixXf& A, double lambda, int k)
{
  int n=256;
  for (int i=0; i<n-2; i++)
    {
      double wij = w(i+1);
      A.coeffRef(k,i) = lambda*wij;
      A.coeffRef(k,i+1) = -2.0*lambda*wij;
      A.coeffRef(k,i+2) = lambda*wij;
      k++;
    } 
}
