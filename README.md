# README #

This is my final project for independent study on Digital Image Processing at the University of Lethbridge, Summer 2016. 

The purpose of this project was to reproduce the experiment in “Recovering High Dynamic Range Radiance Maps from Photographs”  Paul E. Debevec et al. I do so by using the original data set and additional images taken by myself.
As a final part of this project, I investigate changing aperture instead of time, which is what is commonly done in HDR imaging. 

### Image Sets ###

* Memorial - The one from the original paper
* Floor, Yard - Sets from the floor and yard of my place in Lethbridge.
* A  - Sets from an alleyway in downtown Lethbridge; one varies time, the other aperture.
* Can - Sets of images of a can with varying apertures, attempting to maintain similar exposure levels between the sets.

### Code ###

To compile:
	Ensure you have netpbm & check files to make sure the directory is correct
	Ensure you have Eign & check fies to make sure the directory is correct
		http://eigen.tuxfamily.org/index.php?title=Main_Page
	Type make & makefile will take care of it all. 

HDR.h & HDR.cpp are library/functions

HDR.cc 
INPUT: {filename of mid exposure} {number of photographs} {output image filename} {lambda} < {filename of info text file}
OUTPUT: image of radiance map & textfile of g(Z)

HDR2.cc 
//Same as HDR.cc except different method of getting pixel points

RM_to_8bit.cc
INPUT: {filename of image} {1/time} {output image filename} < {filename of g(Z) text file}
OUTPUT: image of radiance map & textfile of lnE

### Steph McIntyre - Summer 2016 ###
